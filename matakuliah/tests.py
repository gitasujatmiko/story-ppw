from django.test import TestCase, Client
from .models import Matkul, Tugas
from .views import matkul, create_matkul, detail_matkul, delete, delete_matkul

class MatkulTestCase(TestCase):
    def setUp(self):
        matkul = Matkul.objects.create(nama_mata_kuliah='PPW', dosen='Meganingrum', \
                jumlah_sks=3, deskripsi='Belajar Django', semester='Gasal', \
                tahun_ajar='2019/2020', ruang='2.2402')
        Tugas.objects.create(mata_kuliah=matkul, tugas='Story6', deadline='2020-03-06 20:00:00')

    def test_matakuliah(self):
        ppw = Matkul.objects.get(nama_mata_kuliah='PPW')
        story6 = Tugas.objects.get(tugas='Story6')
        self.assertEqual(ppw.dosen, 'Meganingrum')
        self.assertEqual(ppw.deskripsi, 'Belajar Django')
        self.assertEqual(ppw.tahun_ajar, '2019/2020')
        self.assertEqual(story6.mata_kuliah.jumlah_sks, 3)

    def test_matakuliah_using_the_right_funcs(self):
        matakuliah_response = Client().get('/matakuliah/')
        self.assertEqual(matakuliah_response.resolver_match.func, matkul)

        create_matkul_response = Client().get('/matakuliah/add/')
        self.assertEqual(create_matkul_response.resolver_match.func, create_matkul)

        detail_matkul_response = Client().get('/matakuliah/1/')
        self.assertEqual(detail_matkul_response.resolver_match.func, detail_matkul)

        delete_response = Client().get('/matakuliah/delete/')
        self.assertEqual(delete_response.resolver_match.func, delete)

        delete_matkul_response = Client().get('/matakuliah/delete/1/')
        self.assertEqual(delete_matkul_response.resolver_match.func, delete_matkul)


    def test_matakuliah_using_the_right_templates(self):
        matakuliah_response = Client().get('/matakuliah/')
        self.assertTemplateUsed(matakuliah_response, 'matakuliah/matkul.html')

        create_matkul_response = Client().get('/matakuliah/add/')
        self.assertTemplateUsed(create_matkul_response, 'matakuliah/matkul-form.html')

        detail_matkul_response = Client().get('/matakuliah/1/')
        self.assertTemplateUsed(detail_matkul_response, 'matakuliah/detail.html')

        delete_response = Client().get('/matakuliah/delete/')
        self.assertTemplateUsed(delete_response, 'matakuliah/delete.html')

        delete_matkul_response = Client().get('/matakuliah/delete/1/')
        self.assertTemplateUsed(delete_matkul_response, 'matakuliah/delete-confirm.html')