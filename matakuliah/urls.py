from django.urls import path, re_path
from . import views

app_name = 'matakuliah'

urlpatterns = [
    path('', views.matkul, name='matkuls'),
    path('add/', views.create_matkul, name='create_matkul'),
    path('<int:matkul_id>/', views.detail_matkul, name='detail_matkul'),
    path('delete/', views.delete, name='delete'),
    path('delete/<int:matkul_id>/', views.delete_matkul, name='delete_matkul'),
]