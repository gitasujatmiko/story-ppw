from django.db import models
from .choices import *

class Matkul(models.Model):

    def __str__(self):
        return self.nama_mata_kuliah

    nama_mata_kuliah = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length=100, choices=SEMESTER_CHOICES, default='Gasal')
    tahun_ajar = models.CharField(max_length=100, choices=TAHUN_CHOICES, default='2019/2020')
    ruang = models.CharField(max_length=100)

class Tugas(models.Model):

    def __str__(self):
        return self.tugas

    mata_kuliah = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    tugas = models.TextField(max_length=200)
    deadline = models.DateTimeField()