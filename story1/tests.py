from django.test import TestCase, Client
from .views import index

class Story1TestCase(TestCase):
    def test_story1_using_the_right_funcs(self):
        story1_response = Client().get('/story1/')
        self.assertEqual(story1_response.resolver_match.func, index)
    
    def test_story1_using_the_right_template(self):
        story1_response = Client().get('/story1/')
        self.assertTemplateUsed(story1_response, 'story1/index.html')
