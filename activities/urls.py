from django.urls import path
from . import views

app_name = 'activities'

urlpatterns = [
    path('', views.activities, name='activities'),
    path('add/', views.add_activity, name='add_activity'),
    path('<int:activity_id>/add-activity-member/', views.add_activity_member, name='add_activity_member'),
    path('delete/<int:member_id>/', views.delete_member, name='delete_member'),
]