from django.shortcuts import render, redirect
from .models import Activity, ActivityMember
from .forms import ActivityForm, ActivityMemberForm

def activities(request):
    activities = Activity.objects.all()
    members = ActivityMember.objects.all()
    return render(request, 'activities/activities.html', {'activities': activities, 'members': members})

def add_activity(request):
    add_activity_form = ActivityForm(request.POST or None)
    if add_activity_form.is_valid():
        add_activity_form.save()
        return redirect('activities:activities')
    return render(request, 'activities/add-activity.html', {'add_activity_form': add_activity_form})

def add_activity_member(request, activity_id):
    activity = Activity.objects.get(id=activity_id)
    add_member_form = ActivityMemberForm(request.POST or None)
    if add_member_form.is_valid():
        add_member = add_member_form.save(commit=False)
        add_member.kegiatan = activity
        add_member.save()
        return redirect('activities:activities')
    return render(request, 'activities/add-activity-member.html', {'add_member_form': add_member_form, 'activity': activity})

def delete_member(request, member_id):
    member = ActivityMember.objects.get(id=member_id)
    if request.method == 'POST':
        member.delete()
        return redirect('activities:activities')

    return render(request, 'activities/activites.html')
    
