from django.test import TestCase, Client
from .models import Activity, ActivityMember
from .views import activities, add_activity, add_activity_member

class ActivityTestCase(TestCase):
    def setUp(self):
        activity = Activity.objects.create(nama_kegiatan='Ngoding', deskripsi_singkat_kegiatan='Yuk ngoding bareng')
        ActivityMember.objects.create(kegiatan=activity, nama_peserta='Gita')

    def test_activity(self):
        ngoding = Activity.objects.get(nama_kegiatan='Ngoding')
        gita = ActivityMember.objects.get(nama_peserta='Gita')
        self.assertEqual(ngoding.nama_kegiatan, 'Ngoding')
        self.assertEqual(gita.kegiatan.deskripsi_singkat_kegiatan, 'Yuk ngoding bareng')

    def test_activities_using_the_right_funcs(self):
        activities_response = Client().get('/activities/')
        self.assertEqual(activities_response.resolver_match.func, activities)

        add_activity_response = Client().get('/activities/add/')
        self.assertEqual(add_activity_response.resolver_match.func, add_activity)

        add_member_response = Client().get('/activities/1/add-activity-member/')
        self.assertEqual(add_member_response.resolver_match.func, add_activity_member)

    def test_activities_using_the_right_templates(self):
        activities_response = Client().get('/activities/')
        self.assertTemplateUsed(activities_response, 'activities/activities.html')

        add_activity_response = Client().get('/activities/add/')
        self.assertTemplateUsed(add_activity_response, 'activities/add-activity.html')

        add_member_response = Client().get('/activities/1/add-activity-member/')
        self.assertTemplateUsed(add_member_response, 'activities/add-activity-member.html')