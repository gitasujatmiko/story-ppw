from django import forms
from .models import Activity, ActivityMember

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['nama_kegiatan', 'deskripsi_singkat_kegiatan']

class ActivityMemberForm(forms.ModelForm):
    class Meta:
        model = ActivityMember
        fields = ['nama_peserta']