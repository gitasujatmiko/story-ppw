from django.db import models

class Activity(models.Model):

    def __str__(self):
        return self.nama_kegiatan

    nama_kegiatan = models.CharField(max_length=150)
    deskripsi_singkat_kegiatan = models.CharField(max_length=300)

class ActivityMember(models.Model):
    
    def __str__(self):
        return self.nama_peserta

    kegiatan = models.ForeignKey(Activity, on_delete=models.CASCADE)
    nama_peserta = models.CharField(max_length=100)