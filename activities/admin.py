from django.contrib import admin
from .models import Activity, ActivityMember

admin.site.register(Activity)
admin.site.register(ActivityMember)