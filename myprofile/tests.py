from django.test import TestCase, Client
from .views import index, projects, time

class MyprofileTestCase(TestCase):
    def test_myprofile_using_the_right_templates(self):
        myprofile_response = Client().get('/')
        self.assertTemplateUsed(myprofile_response, 'myprofile/index.html')

        projects_response = Client().get('/projects/')
        self.assertTemplateUsed(projects_response, 'myprofile/projects.html')

        time_response = Client().get('/time/')
        self.assertTemplateUsed(time_response, 'myprofile/time.html')

    def test_myprofile_using_the_right_funcs(self):
        myprofile_response = Client().get('/')
        self.assertEqual(myprofile_response.resolver_match.func, index)

        projects_response = Client().get('/projects/')
        self.assertEqual(projects_response.resolver_match.func, projects)

        time_response = Client().get('/time/')
        self.assertEqual(time_response.resolver_match.func, time)