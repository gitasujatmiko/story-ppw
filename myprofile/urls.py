from django.urls import path, re_path
from . import views

app_name = 'myprofile'

urlpatterns = [
    path('', views.index, name='index'),
    path('projects/', views.projects, name='projects'),
    path('time/', views.time, name='time'),
    re_path(r'^time/((?P<plus>-?\d+)/)?$', views.time, name='time'),
]