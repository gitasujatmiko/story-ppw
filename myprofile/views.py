from django.shortcuts import render
from django.utils import timezone as tz
from datetime import timedelta

def index(request):
    return render(request, 'myprofile/index.html')

def projects(request):
    return render(request, 'myprofile/projects.html')

def time(request, plus=0):
    tz.activate('Asia/Jakarta')
    now = tz.now()
    time = now + timedelta(hours=int(plus))
    return render(request, 'myprofile/time.html', {'time': time})